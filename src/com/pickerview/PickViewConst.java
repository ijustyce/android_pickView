package com.pickerview;

/**
 * com.pickerview.Const
 * 
 * @author jiyuren <br/>
 *         create at 2015年5月29日 下午5:28:16
 */
public class PickViewConst {
  private static final String TAG = "Const";
  // 取消按钮
  public static int BT_CANCLE_BG = R.drawable.bg_corner_white_blue_selector;
  public static int BT_CANCLE_TEXT = R.color.text_blue_white_selector;
  // 确定按钮
  public static int BT_CONFIRM_BG = R.drawable.bg_corner_blue_white_selector;
  public static int BT_CONFIRM_TEXT = R.color.text_white_blue_selector;

  /**
   * 设置button 红色背景 
   */
  public static void setBtRedStyle() {
    // 取消按钮
    BT_CANCLE_BG = R.drawable.bg_corner_white_red_selector;
    BT_CANCLE_TEXT = R.color.text_red_white_selector;
    // 确定按钮
    BT_CONFIRM_BG = R.drawable.bg_corner_red_white_selector;
    BT_CONFIRM_TEXT = R.color.text_white_red_selector;
  }

}
