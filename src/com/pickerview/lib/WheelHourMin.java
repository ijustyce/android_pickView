package com.pickerview.lib;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;

import com.pickerview.R;

/**
 * 开始时间 --结束时间 com.pickerview.lib.WheelHourMin
 * 
 * @author jiyuren <br/>
 *         create at 2015年5月22日 上午10:20:41
 */
public class WheelHourMin {
  private View view;
  private WheelView startHour;
  private WheelView startMin;
  private WheelView endHour;
  private WheelView endMin;
  public int screenheight;
  private List<String> startHourLs = new ArrayList<String>();
  private List<String> startMinLs = new ArrayList<String>();
  private List<String> endHourLs = new ArrayList<String>();
  private List<String> endMinLs = new ArrayList<String>();
  
  
  public View getView() {
    return view;
  }

  public void setView(View view) {
    this.view = view;
  }

  public WheelHourMin(View view) {
    super();
    this.view = view;
    setView(view);
  }

  public void setTime2TimePicker(List<String> startHourLs, List<String> startMinLs,
      List<String> endHourLs, List<String> endMinLs) {
    this.startHourLs = startHourLs;
    this.startMinLs = startMinLs;
    this.endHourLs = endHourLs;
    this.endMinLs = endMinLs;
    Context context = view.getContext();

    startHour = (WheelView) view.findViewById(R.id.hour_start);
    startHour.setAdapter(new NumericWheelAdapter(startHourLs));
    startHour.setLabel(context.getString(R.string.hours));// 添加文字
    startHour.setCurrentItem(0);

    startMin = (WheelView) view.findViewById(R.id.min_start);
    startMin.setAdapter(new NumericWheelAdapter(startMinLs));
    startMin.setLabel(context.getString(R.string.minutes));// 添加文字
    startMin.setCurrentItem(0);
    
    endHour = (WheelView) view.findViewById(R.id.hour_end);
    endHour.setAdapter(new NumericWheelAdapter(endHourLs));
    endHour.setLabel(context.getString(R.string.hours));// 添加文字
    endHour.setCurrentItem(0);
    
    endMin = (WheelView) view.findViewById(R.id.min_end);
    endMin.setAdapter(new NumericWheelAdapter(endMinLs));
    endMin.setLabel(context.getString(R.string.minutes));// 添加文字
    endMin.setCurrentItem(0);

    // 根据屏幕密度来指定选择器字体的大小(不同屏幕可能不同)
    int textSize = (screenheight / 100) * 3;
    startHour.TEXT_SIZE = textSize;
    startMin.TEXT_SIZE = textSize;
    endHour.TEXT_SIZE = textSize;
    endMin.TEXT_SIZE = textSize;
  }

  /**
   * 设置是否循环滚动
   * 
   * @param cyclic
   */
  public void setCyclic(boolean cyclic) {
    startHour.setCyclic(cyclic);
    startMin.setCyclic(cyclic);
    endHour.setCyclic(cyclic);
    endMin.setCyclic(cyclic);
  }
  
  public void setVisibleItems(int count){
    startHour.setVisibleItems(count);
    startMin.setVisibleItems(count);
    endHour.setVisibleItems(count);
    endMin.setVisibleItems(count);
  }
  /**
   * 获取时间范围
   * @return
   */
  public String[] getTimeRange(){
    String time[] = new String[4];
    time[0] = startHour.getTextItem(startHour.getCurrentItem());
    time[1] = startMin.getTextItem(startMin.getCurrentItem());
    time[2] = endHour.getTextItem(endHour.getCurrentItem());
    time[3] = endMin.getTextItem(endMin.getCurrentItem());
    return time;
  }
}
