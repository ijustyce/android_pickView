package com.pickerview.lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;

import com.pickerview.R;

/**
 * com.jiyuren.fast.widget.WeekCheckView
 * 
 * @author jiyuren <br/>
 *         create at 2015年5月21日 下午6:48:58
 */
public class WeekCheckView extends LinearLayout {
  private static final String TAG = "WeekCheckView";
  private LayoutInflater inflater;
  public static String[] WEEKS = { "日", "一", "二", "三", "四", "五", "六" };
  private int itemWidth = 0;
  private Context context;
  private Map<Integer, String> map = new HashMap<Integer, String>();
  private List<CheckBox> checkBoxLs = new ArrayList<CheckBox>();
  
  public WeekCheckView(Context context) {
    super(context);
    init(context);
  }

  public WeekCheckView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }
  
  /**
   * @param context
   */
  private void init(Context context) {
    this.context = context;
    setBackgroundColor(Color.WHITE);
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT,
        WindowManager.LayoutParams.WRAP_CONTENT);
    setLayoutParams(params);
    setOrientation(LinearLayout.HORIZONTAL);
    setGravity(Gravity.CENTER);
    CheckBox checkView = null;
    DisplayMetrics displayMetrics = new DisplayMetrics();
    ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    int screenWidth = displayMetrics.widthPixels;
    itemWidth = screenWidth / WEEKS.length;
    inflater = LayoutInflater.from(context);

    LinearLayout.LayoutParams cbParams = new LinearLayout.LayoutParams(itemWidth - 20,
        LinearLayout.LayoutParams.MATCH_PARENT);
    cbParams.setMargins(10, 0, 10, 0);
    for (int i = 0; i < WEEKS.length; i++) {
      checkView = (CheckBox) inflater.inflate(R.layout.view_checbox, null);
      checkView.setWidth(itemWidth);
      checkView.setLayoutParams(cbParams);
      this.addView(checkView);
      checkView.setText(WEEKS[i]);
      checkView.setOnCheckedChangeListener(new MyOnCheckChangeListener(i));
      checkBoxLs.add(checkView);
    }
  }
  /**
   * 设置哪些被选中
   * @param index
   */
  public void setWeekChecked(int...index){
    if(!checkBoxLs.isEmpty()){
      for(int position=0;position < index.length;position++){
        checkBoxLs.get(index[position]).setChecked(true);
      }
    }
  }
  class MyOnCheckChangeListener implements OnCheckedChangeListener {
    private int index;

    public MyOnCheckChangeListener(int index) {
      this.index = index;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
      if (isChecked) {
        map.put(index, WEEKS[index]);
      } else {
        map.remove(index);
      }
      if (null != checkListener) {
        checkListener.onWeekCheckListener(map);
      }
    }
  }

  private OnWeekCheckListener checkListener;

  public void setOnWeekCheckListener(OnWeekCheckListener checkListener) {
    this.checkListener = checkListener;
  }

  public interface OnWeekCheckListener {
    public void onWeekCheckListener(Map<Integer, String> map);
  }

}
