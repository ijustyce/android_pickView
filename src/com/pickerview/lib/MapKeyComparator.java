package com.pickerview.lib;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * com.pickerview.lib.MapKeyComparator
 * @author jiyuren <br/>
 * create at 2015年5月26日 下午5:15:02
 */
public class MapKeyComparator implements Comparator<Integer> {
  private static final String TAG = "MapKeyComparator";
  public int compare(Integer index1, Integer index2) {
    return index1.compareTo(index2);
  }
  
  /**
   * 使用 Map按key进行排序
   * 
   * @param map
   * @return
   */
  public static Map<Integer, String> sortMapByKey(Map<Integer, String> map) {
    if (map == null || map.isEmpty()) {
      return null;
    }
    Map<Integer, String> sortMap = new TreeMap<Integer, String>(new MapKeyComparator());
    sortMap.putAll(map);
    return sortMap;
  }
}
