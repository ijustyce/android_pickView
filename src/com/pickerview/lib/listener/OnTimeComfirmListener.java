package com.pickerview.lib.listener;

/**
 * com.pickerview.lib.listener.OnTimeComfirmListener
 * @author jiyuren <br/>
 * create at 2015年5月26日 下午5:13:04
 */
public interface OnTimeComfirmListener {
  public void onTimeConfirm(String week,String weekIndex, String timeRange);
  public void onTimeConfirm(String[] time);
}
