package com.pickerview.lib.listener;

/**
 * com.pickerview.lib.OnOptionsSelectListener
 * @author jiyuren <br/>
 * create at 2015年5月22日 下午3:30:23
 */
public interface OnOptionsSelectListener {
  public void onOptionsSelect(int options1, int options2, int options3);
}
