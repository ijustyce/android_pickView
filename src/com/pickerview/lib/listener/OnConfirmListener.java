package com.pickerview.lib.listener;

/**
 * com.pickerview.lib.listener.OnConfirmListener
 * @author jiyuren <br/>
 * create at 2015年5月26日 下午6:26:18
 * 自定义 dialog 时使用
 */
public interface OnConfirmListener {
  public void onConfirm();
}
